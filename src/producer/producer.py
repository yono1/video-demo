import os
import cv2
import pafy
import time
from kafka import KafkaProducer

# 環境変数
KAFKA_TOPIC = "video" if (os.environ.get("KAFKA_TOPIC") is None) else os.environ.get("KAFKA_TOPIC")
KAFKA_BOOTSTRAP_SERVER = "localhost:9092" if (os.environ.get("KAFKA_BOOTSTRAP_SERVER") is None) else os.environ.get("KAFKA_BOOTSTRAP_SERVER")
VIDEO_URL = "https://youtu.be/3CmwLOgQxIY" if (os.environ.get("VIDEO_URL") is None) else os.environ.get("VIDEO_URL")
FRAME_INTERVAL = 0.03 if (os.environ.get("FRAME_INTERVAL") is None) else os.environ.get("FRAME_INTERVAL")

# Youtubenの動画を取得する準備
video = pafy.new(VIDEO_URL)
best = video.getbest(preftype="mp4")

def publish_camera():
    # Start up producer
    producer = KafkaProducer(bootstrap_servers=[KAFKA_BOOTSTRAP_SERVER])
    # Youtube動画をキャプチャ
    camera = cv2.VideoCapture(best.url)
    camera.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('H', '2', '6', '4'));
    camera.set(cv2.CAP_PROP_BUFFERSIZE, 1);
    while(True):
        _, frame = camera.read()
        _, buffer = cv2.imencode('.jpg', frame,(cv2.IMWRITE_JPEG_QUALITY, 90))
        producer.send(KAFKA_TOPIC, buffer.tobytes())
            
        # reduced load on processor
        time.sleep(float(FRAME_INTERVAL))

if __name__ == '__main__':
    publish_camera()
