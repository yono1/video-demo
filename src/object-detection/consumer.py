import os
from kafka import KafkaConsumer
from kafka import KafkaProducer
import cv2
import numpy as np

# 環境変数
KAFKA_TOPIC = "video" if (os.environ.get("KAFKA_TOPIC") is None) else os.environ.get("KAFKA_TOPIC")
RESULT_TOPIC = "video_anlytics" if (os.environ.get("RESULT_TOPIC") is None) else os.environ.get("RESULT_TOPIC")
KAFKA_BOOTSTRAP_SERVER = "localhost:9092" if (os.environ.get("KAFKA_BOOTSTRAP_SERVER") is None) else os.environ.get("KAFKA_BOOTSTRAP_SERVER")
WEIGHT = 0.6 if (os.environ.get("WEIGHT") is None) else os.environ.get("WEIGHT")

producer = KafkaProducer(bootstrap_servers=[KAFKA_BOOTSTRAP_SERVER])

consumer = KafkaConsumer(
    KAFKA_TOPIC,
    bootstrap_servers=[KAFKA_BOOTSTRAP_SERVER])

# 動体検知結果の画像をRESULT_TOPICへsendする
def publish_camera(frame):
    _, buffer = cv2.imencode('.jpg', frame,(cv2.IMWRITE_JPEG_QUALITY, 90))
    producer.send(RESULT_TOPIC, buffer.tobytes())

# 画像解析して動体検知する
def tracking(frame, avg):

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    if avg is None:
        avg = gray.copy().astype('float')
    
    cv2.accumulateWeighted(gray, avg, WEIGHT)
    frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))

    # デルタ画像を閾値処理を行う
    thresh = cv2.threshold(frameDelta, 3, 255, cv2.THRESH_BINARY)[1]

    # 画像の閾値に輪郭線を入れる
    contours, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    frame = cv2.drawContours(frame, contours, -1, (0, 255, 0), 3)
    return frame, avg

if __name__ == "__main__":
    msg = []
    avg = None

    for msg in consumer:
        input_img = np.frombuffer(msg.value, dtype=np.uint8)

        # decode msg
        input_img = cv2.imdecode(input_img, cv2.IMREAD_UNCHANGED)
        
        frame, avg = tracking(input_img,avg)
        publish_camera(frame)
