```
export CLUSTER_NAME=microshift01

oc new-project ${CLUSTER_NAME}

oc label namespace ${CLUSTER_NAME} cluster.open-cluster-management.io/managedCluster=${CLUSTER_NAME}

oc apply -f managedcluster.yaml
oc apply -f klusterletaddonconfig.yaml

oc get secret ${CLUSTER_NAME}-import -n ${CLUSTER_NAME} -o jsonpath={.data.crds\\.yaml} | base64 --decode > klusterlet-crd.yaml

oc get secret ${CLUSTER_NAME}-import -n ${CLUSTER_NAME} -o jsonpath={.data.import\\.yaml} | base64 --decode > import.yaml
```

```
$ export RH_USERNAME=<レッドハットカスタマーポータルのアカウントのユーザ名>
$ export RH_PASSWORD=<レッドハットカスタマーポータルのアカウントのパスワード>
$ export EMAIL=<登録したメールアドレス>

$ oc project open-cluster-management-agent
$ oc create secret docker-registry open-cluster-management-image-pull-credentials \
    --docker-server=registry.redhat.io \
    --docker-username=${RH_USERNAME} \
    --docker-password=${RH_PASSWORD} \
    --docker-email=${EMAIL}
$ oc secrets link klusterlet open-cluster-management-image-pull-credentials --for=pull
$ oc secrets link klusterlet-registration-sa  open-cluster-management-image-pull-credentials --for=pull
$ oc secrets link klusterlet-work-sa  open-cluster-management-image-pull-credentials --for=pull

$ oc project open-cluster-management-agent-addon 
$ oc create secret docker-registry open-cluster-management-image-pull-credentials \
    --docker-server=registry.redhat.io \
    --docker-username=$RH_USERNAME \
    --docker-password=$RH_PASSWORD \
    --docker-email=$EMAIL

$ oc secrets link cluster-proxy  open-cluster-management-image-pull-credentials --for=pull
$ oc secrets link klusterlet-addon-search  open-cluster-management-image-pull-credentials --for=pull
$ oc secrets link application-manager  open-cluster-management-image-pull-credentials --for=pull
$ oc secrets link klusterlet-addon-workmgr  open-cluster-management-image-pull-credentials --for=pull
```