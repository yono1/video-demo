# video-demo
Youtubeの動画フレームをKafkaへPublishして、Flaskで表示

# Producer
Youtubeの監視カメラ映像をデータソースとして、Kafkaトピックへsend
トピック名: video

## build
```
$ export MY_REGISTRY=<コンテナレジストリのパス>
$ cd src/producer
$ docker build . -t producer:1.0
$ docker tag producer:1.0 ${MY_REGISTRY}/producer:1.0
$ docker push ${MY_REGISTRY}/producer:1.0
```

## Deploy
```
$ oc apply -f manifest/producer
```

# Consumer
Kafka上のvideoトピックのデータをconsumeして、flaskでweb描画。

## Build
```
$ export MY_REGISTRY=<コンテナレジストリのパス>
$ cd src/consumer
$ docker build . -t consumer:1.0
$ docker tag producer:1.0 ${MY_REGISTRY}/consumer:1.0
$ docker push ${MY_REGISTRY}/consumer:1.0
```

## Deploy
```
$ oc apply -f manifest/consumer
$ URL=`oc get route -o jsonpath='{.status.url}'`
```

ブラウザを開いて、http://{$URL} へアクセスすると監視カメラの映像が表示される。

# Tracking
videoトピックのデータをConsumeして、画像解析後の結果をvideo_analyticsトピックとしてsend

## Build
```
$ export MY_REGISTRY=<コンテナレジストリのパス>
$ cd src/tracking
$ docker build . -t tracking:1.0
$ docker tag producer:1.0 ${MY_REGISTRY}/tracking:1.0
$ docker push ${MY_REGISTRY}/tracking:1.0
```

## Deploy
```
$ oc apply -f manifest/tracking
```

Consumerを新たにデプロイするか、RollingUpdateで更新すると、ブラウザ上での表示が変わる。

```
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    app: video-demo-consumer-analytics
  labels:
    app: video-demo-consumer-analytics
  name: consumer-analytics
  namespace: video-demo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: video-demo-consumer-analytics
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      annotations:
        app: video-demo-consumer-analytics
      labels:
        app: video-demo-consumer-analytics
    spec:
      containers:
      - image: registry.gitlab.com/yono1/video-demo/consumer:1.0
        imagePullPolicy: IfNotPresent
        name: consumer
        env:
        - name: KAFKA_TOPIC
          value: "video_analytics"
        - name: KAFKA_BOOTSTRAP_SERVER
          value: "my-cluster-kafka-bootstrap:9092"
        ports:
        - containerPort: 5000
          protocol: TCP
      restartPolicy: Always
---
apiVersion: v1
kind: Service
metadata:
  name: consumer-analytics
  namespace: video-demo
spec:
  selector:
    app: video-demo-consumer-analytics
  ports:
    - protocol: TCP
      port: 5000
      targetPort: 5000
```